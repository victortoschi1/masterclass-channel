import { filterArrayByIndex, filterArrayByString } from '../utils/'

const updateCurrentVideos = (context, tabId) => {
  const videos = context.getters.allVideos
  context.commit('setCurrentVideoList', videos[tabId])
}

const insertCurrentVideoTerms = (context, videos) => {
  const termsList = []
  const peopleList = []

  videos.forEach(video => {
    video.terms.split(', ').forEach(item => termsList.push(item))
    video.people.split(', ').forEach(item => peopleList.push(item))
  })

  const filteredTerms = filterArrayByIndex(termsList)
  const filteredPeople = filterArrayByIndex(peopleList)

  context.commit('setCurrentTagsTerms', filteredTerms)
  context.commit('setCurrentTagsPeople', filteredPeople)
}

const filterVideos = (context, obj) => {
  if (!obj.keyword) {
    context.commit('executeSearch', '')
    context.dispatch('updateCurrentVideos', context.getters.selectedTabId)
    return
  }

  const currentVideoList = context.getters.currentVideoList
  const newVideoList = currentVideoList.filter(curr =>
    filterArrayByString(curr[obj.searchType].split(', '), obj.keyword).length > 0 ? true : false
  )

  context.commit('executeSearch', obj.keyword)
  context.commit('setCurrentVideoList', newVideoList)
}

export default {
  updateCurrentVideos,
  insertCurrentVideoTerms,
  filterVideos
}