export default {
  allVideos: state => state.allVideos,
  currentVideoList: state => state.currentVideos,
  currentTagsPeople: state => state.currentTagsPeople,
  currentTagsTerms: state => state.currentTagsTerms,
  keywordSearch: state => state.keywordSearch,
  selectedTabId: state => state.selectedTabId,
  selectedVideo: state => state.selectedVideo,
  showModal: state => state.showModal,
  tabList: state => state.tabList,
}