export default {
  allVideos: {},
  currentVideos: {},
  currentTagsPeople: [],
  currentTagsTerms: [],
  keywordSearch: '',
  mainChannelData: {},
  selectedTabId: undefined,
  selectedVideo: {},
  showModal: false,
  tabList: []  
}