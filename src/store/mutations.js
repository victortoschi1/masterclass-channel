export default {
  executeSearch (state, keyword) {
    state.keywordSearch = keyword
  },
  setAllVideos (state, obj) {
    state.allVideos = obj
  },
  setCurrentVideoList (state, obj) {
    state.currentVideos = obj
  },
  setCurrentTagsPeople (state, arr) {
    state.currentTagsPeople = arr
  },
  setCurrentTagsTerms (state, arr) {
    state.currentTagsTerms = arr
  },
  setMainChannelData (state, obj) {
    state.mainChannelData = obj
  },
  setSelectedTabId (state, id) {
    state.selectedTabId = id
  },  
  setSelectedVideo (state, obj) {
    state.selectedVideo = obj
  },
  setShowModal (state, bool) {
    state.showModal = bool
  },
  setTabList (state, arr) {
    state.tabList = arr
  }  
}