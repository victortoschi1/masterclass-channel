import * as moment from 'moment'

/**
 * Filter array based in the same index position
 * @function
 * @return {Array} 
 */
const filterArrayByIndex = list => 
  list.filter((element, index, arr) =>
    arr.indexOf(element) === index
)

/**
 * Filter array based in the string param
 * @function
 * @return {Array}
 */
const filterArrayByString = (arr, str) => {
  return arr.filter(item => item.toLowerCase().indexOf(str.toLowerCase()) > -1)
}

/**
 * Split a string and returns an array
 * @function
 * @return {Array}
 */
const splitString = str => str.split(', ')

/**
 * Format date
 * @function
 * @return {String} - March 16, 2018
 */
const formatDate = currDate =>
  moment(currDate.replace(/[/]/g, '-')).format('MMMM DD, YYYY')

export {
  filterArrayByIndex,
  filterArrayByString,
  splitString,
  formatDate
}